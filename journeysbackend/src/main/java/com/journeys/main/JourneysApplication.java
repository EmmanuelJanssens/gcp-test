package com.journeys.main;

import com.journeys.main.model.PointOfInterest;
import com.journeys.main.model.User;
import com.journeys.main.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class JourneysApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(JourneysApplication.class, args);
	}

	private final static Logger log = LoggerFactory.getLogger(JourneysApplication.class);

	@Bean
	CommandLineRunner demo(UserRepository personRepository) {
		return args -> {

			personRepository.deleteAll();

			User greg = new User("TRUC", "Greg", "Lefou", "pass");
			User roy = new User("Machin", "Roy", "Roberton", "pass2");

			PointOfInterest chillon = new PointOfInterest("Château de Chillon");
			PointOfInterest museeDuJeu = new PointOfInterest("Musée suisse du jeu");
			PointOfInterest alimentarium = new PointOfInterest("Alimentarium");

			List<User> team = Arrays.asList(greg, roy);

			personRepository.save(greg);
			personRepository.save(roy);


			greg = personRepository.findByFirstName(greg.getFirstName());
			greg.visitPOI(chillon);
			greg.visitPOI(museeDuJeu);
			personRepository.save(greg);


			roy = personRepository.findByFirstName(roy.getFirstName());
			roy.visitPOI(alimentarium);
			// We already know that roy works with greg
			personRepository.save(roy);

			// We already know craig works with roy and greg

		};
	}

}
