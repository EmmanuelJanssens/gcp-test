package com.journeys.main.model;

import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;

import java.util.HashSet;
import java.util.Set;

@Node
public class User {
    @Id @GeneratedValue private Long id;

    private String nickname;
    private String firstName;
    private String lastName;
    private String password;

    public User() {

    }

    public User(String firstName) {
        this.firstName = firstName;
    }

    public User(String nickname, String firstName, String lastName, String password) {
        this.nickname = nickname;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Relationship(type = "VISITS")
    public Set<PointOfInterest> pointOfInterestSet;

    public void visitPOI(PointOfInterest poi) {
        if(pointOfInterestSet == null) {
            pointOfInterestSet = new HashSet<>();
        }
        pointOfInterestSet.add(poi);
    }

}
