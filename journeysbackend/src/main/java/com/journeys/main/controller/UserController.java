package com.journeys.main.controller;
import com.journeys.main.model.User;
import com.journeys.main.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping(path = "/user/all")
    public Collection<User> getAll() {
        return userService.getAll();
    }

}
