package com.journeys.main.repositories;

import com.journeys.main.model.PointOfInterest;
import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface PointOfInterestRepository extends Neo4jRepository<PointOfInterest, Long> {
    PointOfInterest findByName(String name);

}
