package com.journeys.main.repositories;

import com.journeys.main.model.PointOfInterest;
import com.journeys.main.model.User;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;

import java.util.Collection;
import java.util.List;

public interface UserRepository extends Neo4jRepository<User, Long> {
    User findByFirstName(String firstName);
    List<PointOfInterest> findByPointOfInterestSet(String firstName);
    @Query(value = "MATCH (u:User)-[:VISITS]-> (poi:PointOfInterest) return u, poi")
    Collection<User> getAllUsers();

}
