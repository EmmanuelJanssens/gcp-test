FROM tomcat:9

COPY ./target/main-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/ROOT.war
COPY wait-for-it.sh  /usr/local/tomcat/wait-for-it.sh
RUN chmod +x /usr/local/tomcat/wait-for-it.sh

ENTRYPOINT ["/usr/local/tomcat/wait-for-it.sh","-t","0","journeys-db.journeys-web:7474","--"]

CMD [ "/usr/local/tomcat/bin/catalina.sh","jpda", "run" ]